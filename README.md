![logo](https://i.imgur.com/tjPPO4B.png)

![minigame](https://img.shields.io/badge/DESIGNED%20FOR-MINIGAME%20SERVERS-SALMON?style=for-the-badge)

**Supported Versions:**

![1.8](https://img.shields.io/badge/1.8-UNSUPPORTED-RED?style=for-the-badge) ![1.12](https://img.shields.io/badge/1.12%20to%201.14-SUPPORTED-BLUE?style=for-the-badge) ![1.15.x](https://img.shields.io/badge/1.15+-FULL%20SUPPORT-green?style=for-the-badge)

Retro's Generators are upgradable generators configurable to any block, it includes a handful of standard
generators as well as the ability to create custom generators! Items are stored inside the generators to
reduce lag on the server as well as give for new gameplay mechanics. Players can punch a generatore to explode
the items out in a fancy fountain of items. Generators also display their inventory volume through nice HUDs above
them. Upgrading is simply done by clicking them with Upgrade Tickets!


## Download

**NOTICE**: Versions lower than 1.13 will need to change the `sounds.yml` and the `particles.yml` to supported sounds and particles or disbale them. There are suggested sounds and particles commented out in the config.

Additionally some of the generators provided by default will not work on lower versions, so expect errors trying to spawn a nautilus generator for example on versions that do not support the aquatic update.

[![latest](https://img.shields.io/badge/Download-ALL%20Versions-green?style=for-the-badge&?logoColor=red)](https://songoda.com/marketplace/product/retros-generators-retros-generators.394)


## Build
To build clone the repo and run `mvn`. The compile plugin will be in the `target` directory.

## Generators

Generators can be spawned in using `/generator spawn <generator>` command. Once placed they will
start working immediatly, they can be configured to have any particles you'd like however you'll
need to create your own Generator class from the `AGenerator` abstract class and compile to build 
advanced customization.

You can remove generators with `/generator delete` while looking at a generator.

# Upgrade Tickets

Generators can be upgraded using Upgrade Tickets. Obtain them through `/generator ticket`. By default
upgrade scaling is 2x the generator level. So level 2 will cost 1 tickets, level 3: 4 tickets, and so on.

# Usage on Servers

These generators are designed for building your own mini-games but will work on other servers as well. I
reccomend using a shop plugin to sell the generators and upgrade tickets.

## In-Game Player Usage
#### Obtaining items
To obtain items from a generator, just punch it!

#### Upgrading
Place Upgrade Tickets into a generator to upgrade them. That's it!

## Misc
#### Generator Persistance
Generators will keep their place, level and even item count across restarts.

## Commands & Permissions
Admins will need the `spawn`, `delete` nodes to use obtain generators. Additionally the `ticket` perm is needed to
spawn upgrade tickets.

Spawning Generators `/generator spawn <name>`
```
retrosgenerators.spawn
```
Deleting Generators `/generator delete`
```
retrosgenerators.delete
```
Pickup up Generators `/generator pickup`
```
retrosgenerators.pickup
```
Spawning Upgrade Tickets `/generator ticket`
```
retrosgenerators.ticket
```
Freeze all generators `/generator freeze`
```
retrosgenerators.freeze
```
Make a custom generator `/generator custom <display_block> <output> <(optional) name>`
```
retrosgenerators.custom
```

## Configuration

### General
These values can be confiured to change the defaults of the generators, as well as additional settings like stack size
```yml
item_stack_size: 16 # Amount of items to place in a stack when dispensing
rate: 20 # Default spawn rate in ticks
multiplier: 0.2 # Default multiplier per level
IronGenerator:
    rate: 20
    multiplier: 0.2
GoldGenerator:
    rate: 40
    multiplier: 0.2
DiamondGenerator:
    rate: 80
    multiplier: 0.2
EmeraldGenerator:
    rate: 160
    multiplier: 0.2
NautilusGenerator:
    rate: 160
    multiplier: 0.2
```

### Sounds
Default sounds can be changed in `sounds.yml`

### Language
Support for your own language can be added in `language.yml`

### Particles
Default particles can be changed in `particles.yml`

### Data
You can change generator locations, and other info in the `data.yml` but please do this while the server
is shut down to avoid complications.

## Screenshots

![gif](https://i.imgur.com/LU1u5II.gif)

![gen](https://i.imgur.com/6aC4YdY.png)

![gen](https://i.imgur.com/cijrpDh.png)