package com.gitlab.retropronghorn.retrosgenerators;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.gitlab.retropronghorn.retrosgenerators.commands.GeneratorExecutor;
import com.gitlab.retropronghorn.retrosgenerators.commands.GeneratorTabCompleter;
import com.gitlab.retropronghorn.retrosgenerators.database.Database;
import com.gitlab.retropronghorn.retrosgenerators.helpers.GeneratorStorageHelper;
import com.gitlab.retropronghorn.retrosgenerators.helpers.LoggingHelper;
import com.gitlab.retropronghorn.retrosgenerators.interfaces.IGenerator;
import com.gitlab.retropronghorn.retrosgenerators.items.UpgradeTicket;
import com.gitlab.retropronghorn.retrosgenerators.listeners.BlockBreakListener;
import com.gitlab.retropronghorn.retrosgenerators.listeners.BlockPlaceListener;
import com.gitlab.retropronghorn.retrosgenerators.listeners.PlayerInteractListener;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public final class RetrosGenerators extends JavaPlugin {
    public static RetrosGenerators instance;
    public final UpgradeTicket upgradeTicket = new UpgradeTicket();

    private Boolean freeze = false;
    private FileConfiguration soundsConfig;
    private FileConfiguration particlesConfig;

    public Database db;
    GeneratorStorageHelper generatorStorageHelper;
    public final List<IGenerator> generators = new ArrayList<>();
    public final List<String> generatorTypes = Arrays.asList(
      "IronGenerator",
      "GoldGenerator",
      "DiamondGenerator",
      "EmeraldGenerator",
      "GunpowderGenerator",
      "NautilusGenerator"
    );

    public static JavaPlugin getPlugin() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;
        saveDefaultConfig();

        LoggingHelper.info("Databse loaded.");
        db = new Database(this);
        generatorStorageHelper = new GeneratorStorageHelper(this);

        // Setup commands & completion
        LoggingHelper.info("Commands registered.");
        this.getCommand("generator").setExecutor(new GeneratorExecutor(this));
        this.getCommand("generator").setTabCompleter(new GeneratorTabCompleter(this));

        // Event Listeners
        LoggingHelper.info("Listeneres bound.");
        this.getServer().getPluginManager().registerEvents(new BlockPlaceListener(this), this);
        this.getServer().getPluginManager().registerEvents(new BlockBreakListener(this), this);
        this.getServer().getPluginManager().registerEvents(new PlayerInteractListener(this), this);

        // Register custom configs
        LoggingHelper.info("Hotel? Trivago.");
        registerCustomConfigs();
        generatorStorageHelper.loadGeneratorsFromStorage();
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        LoggingHelper.info("Unloading generators & saving them to storage.");
        generators.forEach(generator -> generator.unload());
    }

    public Boolean isFrozen() {
        return freeze;
    }

    public void toggleFreeze() {
        this.freeze = !this.freeze;
        System.out.println("Frozen?: " + freeze);
    }

    /**
     * Get the sound configuration
     **/
    public FileConfiguration getSoundsConfig() {
        return this.soundsConfig;
    }

     /**
     * Get the particles configuration
     **/
    public FileConfiguration getParticlesConfig() {
        return this.particlesConfig;
    }

    public void registerCustomConfigs() {
        File soundsFile = new File(getDataFolder(), "sounds.yml");
        File particlesFile = new File(getDataFolder(), "particles.yml");

        if (!soundsFile.exists()) {
            soundsFile.getParentFile().mkdirs();
            saveResource("sounds.yml", false);
        }

        if (!particlesFile.exists()) {
            particlesFile.getParentFile().mkdirs();
            saveResource("particles.yml", false);
        }

        soundsConfig = new YamlConfiguration();
        particlesConfig = new YamlConfiguration();

        try {
            soundsConfig.load(soundsFile);
            particlesConfig.load(particlesFile);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    public void registerNewGenerator(IGenerator generator) {
        // Plugin startup logic
        generators.add(generator);
    }
}
