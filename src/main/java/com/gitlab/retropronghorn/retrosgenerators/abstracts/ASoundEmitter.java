package com.gitlab.retropronghorn.retrosgenerators.abstracts;

import java.util.HashMap;

import org.bukkit.Location;
import org.bukkit.Sound;

public abstract class ASoundEmitter {
    public HashMap<String, Sound> sounds = new HashMap<>();

    /**
     * Add a sound to the local library
     * @param sound Sound to play
     * @param key key pointing to the sound to play
     **/
    public void addSound(String sound, String key) {
        Sound s = Sound.valueOf(sound);
        sounds.put(key, s);
    }

    /**
     * Play a sound
     * @param key key pointing to the sound to play
     * @param loc location to play the sound at
     **/
    public void play(String key, Location loc) {
        loc.getWorld().playSound(loc, sounds.get(key), 1f, 1f);
    }
}