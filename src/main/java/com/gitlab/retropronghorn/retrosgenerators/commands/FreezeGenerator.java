package com.gitlab.retropronghorn.retrosgenerators.commands;

import com.gitlab.retropronghorn.retrosgenerators.RetrosGenerators;

public class FreezeGenerator {
    private RetrosGenerators instance;

    public FreezeGenerator(RetrosGenerators instance) {
        this.instance = instance;
    }

    public void toggleFreeze() {
        instance.toggleFreeze();
    }
}