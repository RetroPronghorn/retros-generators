package com.gitlab.retropronghorn.retrosgenerators.commands;

import com.gitlab.retropronghorn.retrosgenerators.RetrosGenerators;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GeneratorExecutor implements CommandExecutor {
    private DeleteGenerator deleteGenerator;
    private PickupGenerator pickupGenerator;
    private GiveGenerator giveGenerator;
    private FreezeGenerator freezeGenerator;
    private GiveUpgradeTicket giveUpgradeTicket;
    private GiveCustomGenerator giveCustomGenrator;

    public GeneratorExecutor(RetrosGenerators instance) {
        this.freezeGenerator = new FreezeGenerator(instance);
        this.deleteGenerator = new DeleteGenerator(instance);
        this.pickupGenerator = new PickupGenerator(instance);
        this.giveGenerator = new GiveGenerator(instance);
        this.giveUpgradeTicket = new GiveUpgradeTicket(instance);
        this.giveCustomGenrator = new GiveCustomGenerator(instance);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = Bukkit.getPlayer(sender.getName());

        if (player.hasPermission("retrosgenerators.delete") || player.isOp()) {
            if (args.length == 1 && args[0].equalsIgnoreCase("delete")) {
                deleteGenerator.delete(sender, command, label, args);
            }
        }

        if (player.hasPermission("retrosgenerators.pickup") || player.isOp()) {
            if (args.length == 1 && args[0].equalsIgnoreCase("pickup")) {
                pickupGenerator.pickup(sender, command, label, args);
            }
        }

        if (player.hasPermission("retrosgenerators.ticket") || player.isOp()) {
            if (args.length == 1 && args[0].equalsIgnoreCase("ticket")) {
                giveUpgradeTicket.give(sender, command, label, args);
            }
        }

        if (player.hasPermission("retrosgenerators.spawn") || player.isOp()) {
            if (args.length >= 2 && args[0].equalsIgnoreCase("spawn")) {
                giveGenerator.give(sender, command, label, args);
            }
        }

        if (player.hasPermission("retrosgenerators.freeze") || player.isOp()) {
            if (args.length >= 1 && args[0].equalsIgnoreCase("freeze")) {
                freezeGenerator.toggleFreeze();
            }
        }

        if (player.hasPermission("retrosgenerators.custom") || player.isOp()) {
            if (args.length >= 1 && args[0].equalsIgnoreCase("custom")) {
                giveCustomGenrator.give(sender, command, label, args);
            }
        }
        
        return false;
    }

}