package com.gitlab.retropronghorn.retrosgenerators.commands;

import java.util.ArrayList;
import java.util.List;

import com.gitlab.retropronghorn.retrosgenerators.RetrosGenerators;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

public class GeneratorTabCompleter implements TabCompleter {
    private RetrosGenerators instance;

    public GeneratorTabCompleter(RetrosGenerators instance) {
        this.instance = instance;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        Player senderPlayer = Bukkit.getPlayer(sender.getName());
        if (command.getName().equalsIgnoreCase("generator")) {
            List<String> commands = new ArrayList<String>();
            if (args.length == 1) {
                if (senderPlayer.hasPermission("generator.delete") || senderPlayer.isOp()) {
                    commands.add("delete");
                }

                if (senderPlayer.hasPermission("generator.pickup") || senderPlayer.isOp()) {
                    commands.add("pickup");
                }

                if (senderPlayer.hasPermission("generator.spawn") || senderPlayer.isOp()) {
                    commands.add("spawn");
                }

                if (senderPlayer.hasPermission("generator.ticket") || senderPlayer.isOp()) {
                    commands.add("ticket");
                }

                if (senderPlayer.hasPermission("generator.freeze") || senderPlayer.isOp()) {
                    commands.add("freeze");
                }

                if (senderPlayer.hasPermission("generator.custom") || senderPlayer.isOp()) {
                    commands.add("custom");
                }
            } else if (args.length == 2) {
                if (args[0].equalsIgnoreCase("spawn")) {
                    return trimListToMatches(instance.generatorTypes, args[1]);
                }
                if (args[0].equalsIgnoreCase("custom")) {
                    List<String> materials = new ArrayList<String>();
                    for (Material material : Material.values()) {
                        materials.add(material.toString());
                    }
                    return trimListToMatches(materials, args[1]);
                }
            } else if (args.length == 3) {
                if (args[0].equalsIgnoreCase("custom")) {
                    List<String> materials = new ArrayList<String>();
                    for (Material material : Material.values()) {
                        materials.add(material.toString());
                    }
                    return trimListToMatches(materials, args[2]);
                }
            }

            return trimListToMatches(commands, args[0]);
        }
        return null;
    }

    private List<String> trimListToMatches(List<String> lst, String query) {
        List<String> newList = new ArrayList<String>();
        lst.forEach(item -> {
            if (StringUtils.containsIgnoreCase(item.toString(), query)) {
                newList.add(item.toString());
            }
        });
        return newList;
    }

}