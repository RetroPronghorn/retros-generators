package com.gitlab.retropronghorn.retrosgenerators.commands;

import java.util.Arrays;

import com.gitlab.retropronghorn.retrosgenerators.RetrosGenerators;
import com.gitlab.retropronghorn.retrosgenerators.generators.CustomGenerator;
import com.gitlab.retropronghorn.retrosgenerators.helpers.MessageHelper;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;

public class GiveCustomGenerator {
    private RetrosGenerators instance;

    public GiveCustomGenerator(RetrosGenerators instance) {
        this.instance = instance;
    }

    /**
     * Give a generator
     * @param sender sender of the command
     * @param command command executed
     * @param label command label
     * @param args arguments given to the command
     **/
    public void give(CommandSender sender, Command command, String label, String[] args) {
        Player player = Bukkit.getPlayer(sender.getName());
        if (args.length >= 3) {
            String material = args[1];
            String outputs = args[2];
            String[] nameArray = Arrays.copyOfRange(args, 3, args.length);
            String name = (args[3] != null) ? String.join(" ", nameArray) : "Custom Generator";
            CustomGenerator customGenerator = new CustomGenerator(
                name,
                20L, // TODO: make this an argument
                1,
                0.25 // TODO: make this an argument
            );
            Boolean materialSuccess = customGenerator.changeMaterial(material);
            Boolean outputSuccess = customGenerator.changeOutput(outputs);
            if (materialSuccess && outputSuccess) {
                customGenerator.bindInstance(instance);
                instance.registerNewGenerator(customGenerator);
                player.getInventory().addItem(customGenerator.getBase());
            } else {
                MessageHelper.sendChatMessage(player, ChatColor.RED + "Inavild material our output type.");
            }
        } else {
            MessageHelper.sendChatMessage(player, ChatColor.YELLOW + "Missing arguments, please check the docs.");
        }
    }

}