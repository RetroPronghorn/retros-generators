package com.gitlab.retropronghorn.retrosgenerators.commands;

import com.gitlab.retropronghorn.retrosgenerators.RetrosGenerators;
import com.gitlab.retropronghorn.retrosgenerators.helpers.GeneratorStorageHelper;
import com.gitlab.retropronghorn.retrosgenerators.helpers.MessageHelper;
import com.gitlab.retropronghorn.retrosgenerators.interfaces.IGenerator;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GiveGenerator {
    private RetrosGenerators instance;

    public GiveGenerator(RetrosGenerators instance) {
        this.instance = instance;
    }

    /**
     * Give a generator
     * @param sender sender of the command
     * @param command command executed
     * @param label command label
     * @param args arguments given to the command
     **/
    public void give(CommandSender sender, Command command, String label, String[] args) {
        Player player = Bukkit.getPlayer(sender.getName());
        String type = args[1];
        String name = (args.length == 3) ? args[2] : type;
        Long rate = RetrosGenerators.getPlugin().getConfig().getLong(type+".rate");
        Double multiplier = RetrosGenerators.getPlugin().getConfig().getDouble(type+".multiplier");
        IGenerator gen = GeneratorStorageHelper.determineGenerator(
            type,
            name,
            rate != 0 ? rate : RetrosGenerators.getPlugin().getConfig().getLong("rate"),
            1,
            multiplier != 0.0 ? multiplier : RetrosGenerators.getPlugin().getConfig().getDouble("multiplier")
        );
        gen.bindInstance(instance);
        instance.registerNewGenerator(gen);
        player.getInventory().addItem(gen.getBase());
    }

}