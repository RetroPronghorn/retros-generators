package com.gitlab.retropronghorn.retrosgenerators.commands;

import com.gitlab.retropronghorn.retrosgenerators.RetrosGenerators;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GiveUpgradeTicket {
    private RetrosGenerators instance;

    /**
     * Construct a GiveUpgradeTicket executor
     * @param RetrosGenerators instace to main plugin
     **/
    public GiveUpgradeTicket(RetrosGenerators instance) {
        this.instance = instance;
    }

    /**
     * Give a upgrade ticket
     * @param sender sender of the command
     * @param command command executed
     * @param label command label
     * @param args arguments given to the command
     **/
    public void give(CommandSender sender, Command command, String label, String[] args) {
        Player player = Bukkit.getPlayer(sender.getName());
        player.getInventory().addItem(instance.upgradeTicket.buildTicket());
    }

}