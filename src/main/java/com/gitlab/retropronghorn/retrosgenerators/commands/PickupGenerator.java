package com.gitlab.retropronghorn.retrosgenerators.commands;

import com.gitlab.retropronghorn.retrosgenerators.RetrosGenerators;
import com.gitlab.retropronghorn.retrosgenerators.interfaces.IGenerator;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;

public class PickupGenerator {
    private RetrosGenerators instance;

    public PickupGenerator(RetrosGenerators instance) {
        this.instance = instance;
    }

    /**
     * Delete a generator
     * @param sender sender of the command
     * @param command command executed
     * @param label command label
     * @param args arguments given to the command
     **/
    public void pickup(CommandSender sender, Command command, String label, String[] args) {
        Player player = Bukkit.getPlayer(sender.getName());
        Location lookingAt = player.getTargetBlock(null, 15).getLocation();
        IGenerator genToPickup = null;
        for (IGenerator generator : instance.generators) {
            if (generator.isPlaced() && lookingAt.toString().equals(generator.getLocation().toString())) {
                genToPickup = generator;
            }
        }
        if (genToPickup != null) {
            player.sendMessage(ChatColor.RED + "Picked up " + genToPickup.getName() + ".");
            genToPickup.pickup();
            player.getWorld().dropItem(
                player.getLocation(),
                genToPickup.getBase()
            );
            instance.db.save();
            lookingAt.getBlock().setType(Material.AIR);
        } else {
            player.sendMessage(ChatColor.YELLOW + "Not looking at a generator.");
        }
    }
}