package com.gitlab.retropronghorn.retrosgenerators.effects;

import java.util.Random;

import com.gitlab.retropronghorn.retrosgenerators.RetrosGenerators;
import com.gitlab.retropronghorn.retrosgenerators.helpers.LocationHelper;
import com.gitlab.retropronghorn.retrosgenerators.helpers.ParticleHelper;
import com.gitlab.retropronghorn.retrosgenerators.helpers.SoundHelper;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

public class ItemFountain {
    private final Long DELAY = 3L;

    private RetrosGenerators instance;
    private String particle;
    private Sound sound;
    private double velocityY;
    private double maxOffset;

    /**
     * Construct an ItemFountain
     * @param RetrosGenerators instace to main plugin
     * @param velocityY velocity in the y index to spawn items
     * @param maxOffset maximum offset from the center to shoot items
     **/
    public ItemFountain(RetrosGenerators instance, String particle, Sound sound, double velocityY, double maxOffset) {
        this.particle = particle;
        this.sound = sound;
        this.instance = instance;
        this.velocityY = velocityY;
        this.maxOffset = maxOffset;
    }

    /**
     * Spawn new itemstacks into the world
     *
     * @param loc Location to spawn items
     * @param item Item to spawn
     * @param count Amount of items to spawn
     **/
    public void spawnItems(Location loc, ItemStack item, int count) {
        spawnItems(loc, item, count, item.getMaxStackSize());
    }

    /**
     * Spawn new itemstacks into the world
     *
     * @param loc Location to spawn items
     * @param item Item to spawn
     * @param count Amount of items to spawn
     * @param maxItemStack maximum amount of items to group when spawning
     **/
    public void spawnItems(Location loc, ItemStack item, int count, int maxItemStack) {
        Random rand = new Random();
        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(instance, new Runnable() {
            public void run() {
                int stackSize = count > maxItemStack ? maxItemStack : count;
                item.setAmount(stackSize);
                Item droppedItem = loc.getWorld().dropItem(loc, item);
                for (int i=0; i<instance.getParticlesConfig().getInt("generator.count"); i++)
                    doParticles(loc);
                droppedItem.setVelocity(
                    new Vector(
                        -maxOffset + (maxOffset - -maxOffset) * rand.nextDouble(),
                        velocityY,
                        -maxOffset + (maxOffset - -maxOffset) * rand.nextDouble()
                    )
                );
                SoundHelper.playSound(sound, loc);
                if (count - stackSize > 0)
                    spawnItems(loc, item, count - stackSize);
            }
        }, DELAY);
    }

    /**
     * Spawn a new particle in the world
     *
     * @param loc location to spawn the particles
     **/
    private void doParticles(Location loc) {
        Vector offset = new Vector(0, 1, 0);
        ParticleHelper.spawnParticle(
            particle,
            LocationHelper.toCenterLocation(loc).add(offset)
        );
    }
}