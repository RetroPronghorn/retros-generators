package com.gitlab.retropronghorn.retrosgenerators.generators;

import com.gitlab.retropronghorn.retrosgenerators.RetrosGenerators;
import com.gitlab.retropronghorn.retrosgenerators.abstracts.AGenerator;
import com.gitlab.retropronghorn.retrosgenerators.interfaces.IGenerator;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.inventory.ItemStack;

public class CustomGenerator extends AGenerator {
    /**
     * Construct an CustomGenerator
     * @param name Name of the generator
     * @param tickRate rate to generate items (in ticks)
     * @param level level of the generator
     * @param multiplier multiplication amount per level
     **/
    public CustomGenerator(String name, Long tickRate, Integer level, Double multiplier) {
        super(name, tickRate, level, multiplier);

        // Set materials
        baseBlock = new ItemStack(material);
        attachMeta();
    }

    public Boolean changeMaterial(String material) {
        if (Material.valueOf(material) != null) {
            this.material = Material.valueOf(material);
            baseBlock = new ItemStack(Material.valueOf(material));
            attachMeta();
            return true;
        }
        return false;
    }

    public Boolean changeOutput(String material) {
        if (Material.valueOf(material) != null) {
            this.generates = Material.valueOf(material);
            return true;
        }
        return false;
    }

    private Boolean setSoundIfExists(String key, String sound) {
        if (Sound.valueOf(sound) != null) {
            Sound s = Sound.valueOf(sound);
            sounds.put(key, s);
            return true;
        }
        return false;
    }

    @Override
    public IGenerator bindInstance(RetrosGenerators instance) {
        super.bindInstance(instance);
        instance.db.set(getUUID().toString()+".custom", true);
        instance.db.save();
        return (IGenerator) this;
    }

    public Boolean changeDispenseSound(String sound) {
        return setSoundIfExists("dispense", sound);
    }

    public Boolean changeSpewSound(String sound) {
        return setSoundIfExists("full", sound);
    }

    public Boolean changeUpgradeSound(String sound) {
        return setSoundIfExists("upgrade", sound);
    }

    @Override
    public Boolean generate() {
        generateItem();
        return true;
    }
}