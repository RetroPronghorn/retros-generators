package com.gitlab.retropronghorn.retrosgenerators.generators;

import com.gitlab.retropronghorn.retrosgenerators.abstracts.AGenerator;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;


public class EmeraldGenerator extends AGenerator {

    /**
     * Construct an EmeraldGenerator
     * @param name Name of the generator
     * @param tickRate rate to generate items (in ticks)
     * @param level level of the generator
     * @param multiplier multiplication amount per level
     **/
    public EmeraldGenerator(String name, Long tickRate, Integer level, Double multiplier) {
        super(name, tickRate, level, multiplier);

        // Set materials
        name = "Emerald Generator";
        material = Material.EMERALD_BLOCK;
        generates = Material.EMERALD;
        baseBlock = new ItemStack(material);
        attachMeta();
    }

    @Override
    public Boolean generate() {
        generateItem();
        return null;
    }

}