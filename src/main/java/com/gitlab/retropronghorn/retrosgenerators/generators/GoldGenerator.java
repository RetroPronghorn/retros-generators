package com.gitlab.retropronghorn.retrosgenerators.generators;

import com.gitlab.retropronghorn.retrosgenerators.abstracts.AGenerator;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;


public class GoldGenerator extends AGenerator {

    /**
     * Construct an GoldGenerator
     * @param name Name of the generator
     * @param tickRate rate to generate items (in ticks)
     * @param level level of the generator
     * @param multiplier multiplication amount per level
     **/
    public GoldGenerator(String name, Long tickRate, Integer level, Double multiplier) {
        super(name, tickRate, level, multiplier);

        // Set materials
        name = "Gold Generator";
        material = Material.GOLD_BLOCK;
        generates = Material.GOLD_INGOT;
        baseBlock = new ItemStack(material);
        attachMeta();
    }

    @Override
    public Boolean generate() {
        generateItem();
        return null;
    }

}