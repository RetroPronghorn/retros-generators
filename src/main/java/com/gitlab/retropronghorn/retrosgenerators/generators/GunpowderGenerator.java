package com.gitlab.retropronghorn.retrosgenerators.generators;

import java.util.Arrays;
import java.util.List;

import com.gitlab.retropronghorn.retrosgenerators.RetrosGenerators;
import com.gitlab.retropronghorn.retrosgenerators.abstracts.AGenerator;
import com.gitlab.retropronghorn.retrosgenerators.interfaces.IGenerator;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;


public class GunpowderGenerator extends AGenerator {

    /**
     * Construct an DragonGenerator
     * @param name Name of the generator
     * @param tickRate rate to generate items (in ticks)
     * @param level level of the generator
     * @param multiplier multiplication amount per level
     **/
    public GunpowderGenerator(String name, Long tickRate, Integer level, Double multiplier) {
        super(name, tickRate, level, multiplier);

        // Set materials
        name = "Gunpowder Generator";
        material = Material.TNT;
        generates = Material.GUNPOWDER;
        baseBlock = new ItemStack(material);
        attachMeta();
    }

    @Override
    public List<String> getParticles() {
        return Arrays.asList("DRIP_LAVA", "EXPLOSION_NORMAL");
    }

    @Override
    public IGenerator bindInstance(RetrosGenerators instance) {
        super.bindInstance(instance);
        addSound("ENTITY_TNT_PRIMED", "dispense");
        addSound("ENTITY_GENERIC_EXPLODE", "full");
        return (IGenerator) this;
    }

    @Override
    public Boolean generate() {
        generateItem();
        return null;
    }

}