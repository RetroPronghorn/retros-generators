package com.gitlab.retropronghorn.retrosgenerators.generators;

import java.util.Arrays;
import java.util.List;

import com.gitlab.retropronghorn.retrosgenerators.RetrosGenerators;
import com.gitlab.retropronghorn.retrosgenerators.abstracts.AGenerator;
import com.gitlab.retropronghorn.retrosgenerators.interfaces.IGenerator;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;


public class NautilusGenerator extends AGenerator {

    /**
     * Construct an NautilusGenerator
     * @param name Name of the generator
     * @param tickRate rate to generate items (in ticks)
     * @param level level of the generator
     * @param multiplier multiplication amount per level
     **/
    public NautilusGenerator(String name, Long tickRate, Integer level, Double multiplier) {
        super(name, tickRate, level, multiplier);

        // Set materials
        name = "Nautilus Generator";
        material = Material.SEA_LANTERN;
        generates = Material.NAUTILUS_SHELL;
        baseBlock = new ItemStack(material);
        attachMeta();
    }

    @Override
    public List<String> getParticles() {
        return Arrays.asList("NAUTILUS", "SQUID_INK");
    }

    @Override
    public IGenerator bindInstance(RetrosGenerators instance) {
        super.bindInstance(instance);
        addSound("BLOCK_BUBBLE_COLUMN_BUBBLE_POP", "dispense");
        addSound("BLOCK_CONDUIT_ACTIVATE", "full");
        return (IGenerator) this;
    }

    @Override
    public Boolean generate() {
        generateItem();
        return null;
    }

}