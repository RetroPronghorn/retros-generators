package com.gitlab.retropronghorn.retrosgenerators.helpers;

import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;

public class DisplayHelper {

    private ArmorStand displayStand;

    /**
     * Construct a new Display Helper
     *
     * @param location Location to spawn the display
     **/
    public DisplayHelper(Location location) {
        displayStand = (ArmorStand) location.getWorld().spawnEntity(
            LocationHelper.toCenterLocation(location),
            EntityType.ARMOR_STAND
        );
        displayStand.setVisible(false);
        if (ServerVersion.isServerVersionAtLeast(ServerVersion.V1_12)) {
            displayStand.setCollidable(false);
            displayStand.setCustomNameVisible(true);
            displayStand.setBasePlate(false);
            displayStand.setSmall(true);
            displayStand.setGravity(false);
            displayStand.setArms(false);
            displayStand.setInvulnerable(true);
        }
    }

    /**
     * Set the text display on this stand
     *
     * @param text Text for the armorstand display
     **/
    public void setText(String text) {
        displayStand.setCustomName(text);
        displayStand.setCustomNameVisible(false);
        displayStand.setCustomNameVisible(true);
    }

    public void moveTo(Location loc) {
        displayStand.teleportAsync(loc);
    }

    /**
     * Delete a amrorstand display
     **/
    public void delete() {
        displayStand.remove();
    }
}