package com.gitlab.retropronghorn.retrosgenerators.helpers;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import com.gitlab.retropronghorn.retrosgenerators.RetrosGenerators;
import com.gitlab.retropronghorn.retrosgenerators.generators.CustomGenerator;
import com.gitlab.retropronghorn.retrosgenerators.generators.DiamondGenerator;
import com.gitlab.retropronghorn.retrosgenerators.generators.EmeraldGenerator;
import com.gitlab.retropronghorn.retrosgenerators.generators.GoldGenerator;
import com.gitlab.retropronghorn.retrosgenerators.generators.GunpowderGenerator;
import com.gitlab.retropronghorn.retrosgenerators.generators.IronGenerator;
import com.gitlab.retropronghorn.retrosgenerators.generators.NautilusGenerator;
import com.gitlab.retropronghorn.retrosgenerators.interfaces.IGenerator;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;

public class GeneratorStorageHelper {

    RetrosGenerators instance;
    static FileConfiguration generators;

    public GeneratorStorageHelper(RetrosGenerators instance) {
        this.instance = instance;
        final File generatorsFile = new File(instance.getDataFolder(), "data.yml");
        if (!generatorsFile.exists()) {
            generatorsFile.getParentFile().mkdirs();
            instance.saveResource("data.yml", false);
        }
        generators = new YamlConfiguration();
        try {
            generators.load(generatorsFile);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    public Boolean isStoredGenerator(ItemStack item, UUID uuid) {
        String parsedUUID = StringColorParser.getUUID("Info:", item);
        if (instance.db.keys().contains(parsedUUID)) {
            return parsedUUID.equals(uuid.toString());
        }
        return instance.db.keys().contains(parsedUUID);
    }

    public void loadGeneratorsFromStorage() {
        generators.getKeys(false).forEach(uuid -> loadGenerator(uuid));
    }

    public void loadGenerator(String uuid) {
        final ConfigurationSection gen = generators.getConfigurationSection(uuid);
        if (!gen.getBoolean("deleted")) {
            IGenerator generator;
            if (!gen.getBoolean("custom")) {
                generator = determineGenerator(
                    gen.getString("type"),
                    gen.getString("name"),
                    gen.getLong("rate"),
                    gen.getInt("level"),
                    gen.getDouble("multiplier")
                );
            } else {
                CustomGenerator customGenerator = new CustomGenerator(
                    gen.getString("name"),
                    gen.getLong("rate"),
                    gen.getInt("level"),
                    gen.getDouble("multiplier")
                );
                customGenerator.changeMaterial(gen.getString("material"));
                customGenerator.changeOutput(gen.getString("generates"));
                generator = customGenerator;
            }
            generator.bindUUID(UUID.fromString(uuid));
            generator.bindInstance(instance);
            instance.registerNewGenerator(generator);
            if (gen.getBoolean("placed")) {
                final ConfigurationSection location = (ConfigurationSection) gen.getConfigurationSection("location");
                final Location loc = new Location(
                    Bukkit.getWorld(location.getString("w")),
                    location.getDouble("x"),
                    location.getDouble("y"),
                    location.getDouble("z")
                );
                generator.place(loc);
                generator.setStoredItems(gen.getInt("stored"));
            }
        }
    }

    public void loadCustomGenerator(ConfigurationSection gen) {
        CustomGenerator customGenerator = new CustomGenerator(
            gen.getString("name"),
            gen.getLong("rate"),
            gen.getInt("level"),
            gen.getDouble("multiplier")
        );
        customGenerator.changeMaterial(gen.getString("material"));
        customGenerator.changeOutput(gen.getString("generates"));
    }

    /**
     * Determine the generator type, and crate the generator
     * @param type type of generator
     * @param name name of the generator
     * @param tickRate generation rate in ticks
     * @param level level of the generator
     * @param multiplier multiply generation rate by level
     * @return returns the generator interface to the generator class
     */
    public static IGenerator determineGenerator(final String type, final String name, final Long tickRate, final Integer level, final Double multiplier) {
        switch(type) {
            case "IronGenerator":
                return new IronGenerator(name, tickRate, level, multiplier);
            case "GoldGenerator":
                return new GoldGenerator(name, tickRate, level, multiplier);
            case "DiamondGenerator":
                return new DiamondGenerator(name, tickRate, level, multiplier);
            case "EmeraldGenerator":
                return new EmeraldGenerator(name, tickRate, level, multiplier);
            case "GunpowderGenerator":
                return new GunpowderGenerator(name, tickRate, level, multiplier);
            case "NautilusGenerator":
                return new NautilusGenerator(name, tickRate, level, multiplier);
            default:
                return null;
        }
    }
}