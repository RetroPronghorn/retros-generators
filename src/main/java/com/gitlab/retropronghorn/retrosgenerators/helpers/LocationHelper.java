package com.gitlab.retropronghorn.retrosgenerators.helpers;

import org.bukkit.Location;

public class LocationHelper {

    public static Location toCenterLocation(Location loc) {
        Location centeredLocation = new Location(
            loc.getWorld(),
                loc.getX()+0.5,
                loc.getY(),
                loc.getZ()+0.5
            );
        return centeredLocation;
    }
}