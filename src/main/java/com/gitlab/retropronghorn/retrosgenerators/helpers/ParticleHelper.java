package com.gitlab.retropronghorn.retrosgenerators.helpers;

import org.bukkit.Location;
import org.bukkit.Particle;

/** Represents a new ParticleHandler
 * @author RetroPronghorn
 * @author https://gitlab.com/retropronghorn/retros-generators
 * @version 1.0-SNAPSHOT
 * @since 1.0
 */
public class ParticleHelper {

    /**
     * Spawn a new particle in the world
     *
     * @param type Particle name
     * @param location Location to spawn the particle
     **/
    public static void spawnParticle(String type, Location location) {
        spawnParticle(type, location, 1);
    }

    /**
     * Spawn a new particle in the world
     *
     * @param type Particle name
     * @param location Location to spawn the particle(s)
     * @param count Amount of particles to spawn
     **/
    public static void spawnParticle(String type, Location location, Integer count) {
        // TODO: Add support for particles in lower versions
        if (ServerVersion.isServerVersionAtLeast(ServerVersion.V1_12))
            location.getWorld().spawnParticle(Particle.valueOf(type), location.getX(), location.getY(), location.getZ(), count);
    }
}
