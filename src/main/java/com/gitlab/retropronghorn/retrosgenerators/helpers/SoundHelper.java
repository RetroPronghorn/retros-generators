package com.gitlab.retropronghorn.retrosgenerators.helpers;

import org.bukkit.Location;
import org.bukkit.Sound;

public class SoundHelper {

    /**
     * Play a sound in the world at specific location
     *
     * @param sound Name of the sound
     * @param world The world to play the sound in
     * @param location Location in the world to play the sound
     **/
    public static void playSound(String sound, Location location) {
        location.getWorld().playSound(location, Sound.valueOf(sound), 1f, 1f);
    }

    public static void playSound(Sound sound, Location location) {
        location.getWorld().playSound(location, sound, 1f, 1f);
    }

}