package com.gitlab.retropronghorn.retrosgenerators.helpers;

import org.bukkit.inventory.ItemStack;

public class StringColorParser {

    public static boolean unifiedColor = true;

    /**
     * Retrun a string with a hidden UUID
     * @param uuid uuid string to hide
     * @return
     */
    public static String uuidToColor(String uuid){
        StringBuilder build = new StringBuilder();
        for(int i = 0; i < uuid.length(); i++){
            if(uuid.charAt(i)!='-')
                build.append("§").append(uuid.charAt(i));
        }
        if(unifiedColor)
                build.append("§5");
        return build.toString();
    }

    /**
     * Returns a uuid string from item lore
     * @param lang Lore line to parse from
     * @param stack item to parse uuid from
     * @return string version of uuid
     */
    public static String getUUID(String lang, ItemStack stack){
        String uuidString = null;
        if(stack == null || stack.getItemMeta() == null || stack.getItemMeta().getLore()==null)
            return null;
        for(String s : stack.getItemMeta().getLore()) {
            if (s.contains(lang))
                uuidString = s;
        }
        if(uuidString!=null){
            String[] sub = uuidString.split(lang);
            String uuid = sub[0].replace("§", "");
            if(unifiedColor)
                uuid = uuid.substring(0, uuid.length()-1);
            uuidString = new StringBuilder(uuid)
                    .insert(8, "-").insert(13, "-").insert(18, "-").insert(23, "-").toString();
        }
        return uuidString;
    }
}
