package com.gitlab.retropronghorn.retrosgenerators.items;

import java.util.Arrays;
import java.util.List;

import com.gitlab.retropronghorn.retrosgenerators.abstracts.AGenerator;
import com.gitlab.retropronghorn.retrosgenerators.helpers.MessageHelper;
import com.gitlab.retropronghorn.retrosgenerators.helpers.StringColorParser;
import com.gitlab.retropronghorn.retrosgenerators.interfaces.IGenerator;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.md_5.bungee.api.ChatColor;

public class UpgradeTicket {
    // TODO: Move this to language file
    final String name = "Upgrade Ticket";
    final String uuid = "7719673b-e652-48c1-ac22-2c0af0cb825c";
    protected Material material = Material.PAPER;

    /**
     * Upgrade a generator by one level
     * @param generator generator to upgrade
     * @return
     */
    public Boolean upgradeGenerator(IGenerator generator, Player player) {
        AGenerator gen = (AGenerator) generator;
        if (!gen.isMaxLevel()) {
            int ticketCount = player.getInventory().getItemInMainHand().getAmount();
            if (ticketCount >= gen.getUpgradeCost()) {
                ItemStack tickets = buildTicket();
                tickets.setAmount(gen.getUpgradeCost());
                player.getInventory().removeItem(tickets);
                // TODO move this to language file
                MessageHelper.sendActionbarMessage(player, ChatColor.GREEN + "Upgrade Ticket Applied");
                gen.play(
                    "upgrade",
                    gen.getLocation()
                );
                return gen.levelUp();
            } else {
                MessageHelper.sendActionbarMessage(
                    player,
                    ChatColor.YELLOW + "Generator needs " + gen.getUpgradeCost() + " tickets"
                );
                return false;
            }
        } else {
            MessageHelper.sendActionbarMessage(player, ChatColor.RED + "Generator is max level");
        }
        return false;
    }

    /**
     * Build a ticket item
     * @return upgrade ticket item
     */
    public ItemStack buildTicket() {
        ItemStack item = new ItemStack(material);
        ItemMeta itemMeta = getMeta(item);
        itemMeta.addEnchant(Enchantment.DURABILITY, 1, true);
        itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        item.setItemMeta(itemMeta);
        return item;
    }

    /**
     * Build and return the meta fro the item
     * @param item the item to apply the meta on
     * @return built item meta
     */
    public ItemMeta getMeta(ItemStack item) {
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        List<String> lore = Arrays.asList(
            StringColorParser.uuidToColor(uuid) +
                "Increases production",
            "Right-click on generator"
        );
        meta.setLore(lore);
        return meta;
    }

    /**
     * Determine if an item is an upgrade ticket
     * @param item item to check against
     * @return wether or not the item is an upgrade ticket
     */
    public Boolean isUpgradeTicket(ItemStack item) {
        if (item == null || item.getType().toString() != "PAPER")
            return false;
        String uuid = StringColorParser.getUUID(
            "Increases production",
            item
        );
        return uuid != null ? uuid.equals(this.uuid) : false;
    }
}