package com.gitlab.retropronghorn.retrosgenerators.listeners;

import com.gitlab.retropronghorn.retrosgenerators.RetrosGenerators;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class BlockBreakListener implements Listener {
    private RetrosGenerators instance;

    /**
     * Constructs a new BlockBreakListener
     * @param instance reference to main plugin
     */
    public BlockBreakListener(RetrosGenerators instance) {
        this.instance = instance;
    }

    @EventHandler
    public void blockBreakEvent(BlockBreakEvent event) {
        instance.generators.forEach(generator -> {
            if (generator.getBase().getType() == event.getBlock().getType() &&
                generator.isGenerator(event.getBlock())) {
                event.getBlock().getDrops().clear();
                generator.pickup();
                event.getBlock().getWorld().dropItem(
                    event.getBlock().getLocation(),
                    generator.getBase()
                );
                return;
            }
        });
    }


}