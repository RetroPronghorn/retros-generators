package com.gitlab.retropronghorn.retrosgenerators.listeners;

import com.gitlab.retropronghorn.retrosgenerators.RetrosGenerators;
import com.gitlab.retropronghorn.retrosgenerators.helpers.GeneratorStorageHelper;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

public class BlockPlaceListener implements Listener {
    private RetrosGenerators instance;
    private GeneratorStorageHelper generatorStorageHelper;
    /**
     * Constructs a new BlockPlaceListener
     * @param instance reference to main plugin
     */
    public BlockPlaceListener(RetrosGenerators instance) {
        this.instance = instance;
        generatorStorageHelper = new GeneratorStorageHelper(instance);
    }

    @EventHandler
    public void blockPlaceEvent(BlockPlaceEvent event) {
        instance.generators.forEach(generator -> {
            if (generator.isGenerator(event.getItemInHand()) ||
                generatorStorageHelper.isStoredGenerator(event.getItemInHand(), generator.getUUID())) {
                generator.place(event.getBlock().getLocation());
                // Remove gen in creative mode
                if (event.getPlayer().getInventory().contains(generator.getBase())) {
                    event.getPlayer().getInventory().removeItem(generator.getBase());
                }
                return;
            }
        });
    }


}