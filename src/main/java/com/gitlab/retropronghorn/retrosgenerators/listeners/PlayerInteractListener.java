package com.gitlab.retropronghorn.retrosgenerators.listeners;

import com.gitlab.retropronghorn.retrosgenerators.RetrosGenerators;
import com.gitlab.retropronghorn.retrosgenerators.helpers.MessageHelper;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

import net.md_5.bungee.api.ChatColor;

public class PlayerInteractListener implements Listener {
    private RetrosGenerators instance;

    /**
     * Constructs a new PlayerInteractListener
     * @param instance reference to main plugin
     */
    public PlayerInteractListener(RetrosGenerators instance) {
        this.instance = instance;
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getHand() == EquipmentSlot.OFF_HAND) return; // off hand packet, ignore.
        instance.generators.forEach(generator -> {
            if (event.getClickedBlock() != null && generator.isPlaced() && generator.isGenerator(event.getClickedBlock())) {
                event.setCancelled(true);
                if (event.getAction().toString().equals("LEFT_CLICK_BLOCK")) {
                    // TODO: move this to language file
                    MessageHelper.sendActionbarMessage(event.getPlayer(), ChatColor.GREEN + "Generator was depleted");
                    generator.dispense();
                } else if (event.getAction().toString().equals("RIGHT_CLICK_BLOCK")) {
                    ItemStack heldItem = event.getPlayer().getInventory().getItemInMainHand();
                    if (heldItem.getType().toString().equals("PAPER") && instance.upgradeTicket.isUpgradeTicket(heldItem)) {
                        instance.upgradeTicket.upgradeGenerator(generator, event.getPlayer());
                    }
                }
                return;
            }
        });
    }
}